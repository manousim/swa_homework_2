# Scoring Calculator
Run with `docker-compose up`

## Consumer
### main.py
Consumes messages, verifies against CRM. Produces ClientScoringCalculated.


## Producer
### send_some_commands.py
Just for testing. Produces sample data for consumer.
