import json
import os
import random
import logging
from time import sleep

from kafka import KafkaProducer

kafka = os.getenv('KAFKA', 'localhost:9092"')
logger = logging.getLogger('ScoringCalculation')
logger.setLevel(logging.INFO)
logging.basicConfig(format='[%(asctime)s] - [%(name)s] - [%(levelname)s] : %(message)s', level=logging.INFO)
producer = KafkaProducer(bootstrap_servers=kafka)


def send_message(client, amount):
    producer.send('CalculateClientScoring', json.dumps({'amount': amount, 'client': client}).encode('utf-8'))


if __name__ == '__main__':
    while True:
        for i in range(1, 6):
            value = random.randint(10000, 5000000)
            logger.info(f'Sending message client: {i}. value: {value}')
            send_message(i, value)
            sleep(4)
        producer.flush()
