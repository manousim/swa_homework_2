import json
import logging
import os
from time import sleep

import requests
from kafka import KafkaProducer, KafkaConsumer
from kafka.errors import NoBrokersAvailable

logger = logging.getLogger('ScoringCalculation')

logger.setLevel(logging.INFO)
logging.basicConfig(format='[%(asctime)s] - [%(name)s] - [%(levelname)s] : %(message)s', level=logging.INFO)


def main_loop():
    for message in consumer:
        logger.info(message)
        data = json.loads(message.value.decode('utf-8'))
        r = requests.get(f"{CRM_API}{data['client']}")
        r = json.loads(r.text)
        client_data = r['clientData']
        logger.info(r)
        accepted = True if (client_data['income'] - client_data['expense']) * 60 > data['amount'] else False
        logger.info(f'Loan is accepted : {accepted}')
        data['result'] = accepted
        producer.send('ClientScoringCalculated', json.dumps(data).encode('utf-8'))


if __name__ == '__main__':
    kafka = os.getenv('KAFKA', 'localhost:9092"')
    crm = os.getenv('CRM', 'localhost:8081')

    logger.info('Connecting to Kafka: {0}'.format(kafka))
    logger.info('Connecting to CRM: {0}'.format(crm))
    CRM_API = f'http://{crm}/api/v1/client/'
    error = None
    for i in range(0, 10):
        try:
            producer = KafkaProducer(bootstrap_servers=kafka)
            consumer = KafkaConsumer("CalculateClientScoring", bootstrap_servers=kafka)
            error = None
            break
        except NoBrokersAvailable as e:
            error = e
            logger.warning(e)
            sleep(4)

    if error is not None:
        logger.error(error)
        exit(1)

    main_loop()
